object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'GOL Generation: 1'
  ClientHeight = 480
  ClientWidth = 640
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbFPS: TLabel
    Left = 0
    Top = 0
    Width = 640
    Height = 13
    Align = alTop
    Caption = 'FPS: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clFuchsia
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ExplicitWidth = 25
  end
  object Panel1: TPanel
    Left = 0
    Top = 13
    Width = 640
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 0
    ExplicitHeight = 480
  end
  object sdFile: TSaveDialog
    DefaultExt = 'gol'
    Filter = 'Game of Life|*.gol'
    Left = 16
    Top = 16
  end
  object odFile: TOpenDialog
    Filter = 'Game of Life|*.gol'
    Left = 16
    Top = 72
  end
  object ActionList1: TActionList
    Left = 256
    Top = 96
    object actStep: TAction
      Caption = '&Evolve'
      ShortCut = 16453
      OnExecute = actStepExecute
    end
    object actPlay: TAction
      Caption = '&Play'
      ShortCut = 16464
      OnExecute = actPlayExecute
    end
    object actPause: TAction
      Caption = '&Pause'
      ShortCut = 16464
      OnExecute = actPlayExecute
    end
    object actRandom: TAction
      Caption = '&Random'
      ShortCut = 16466
      OnExecute = actRandomExecute
    end
    object actSave: TAction
      Caption = '&Save'
      ShortCut = 16467
      OnExecute = actSaveExecute
    end
    object actLoad: TAction
      Caption = '&Open'
      ShortCut = 16463
      OnExecute = actLoadExecute
    end
    object actRules: TAction
      Caption = 'R&ules'
      ShortCut = 16469
      OnExecute = actRulesExecute
    end
    object actOptions: TAction
      Caption = 'Op&tions'
      ShortCut = 16468
    end
    object actClose: TAction
      Caption = 'E&xit'
      OnExecute = actCloseExecute
    end
    object actBenchmark: TAction
      Caption = 'Benchmark'
      OnExecute = actBenchmarkExecute
    end
    object actEdit: TAction
      Caption = 'E&dit'
      ShortCut = 16452
      OnExecute = actEditExecute
    end
    object actClear: TAction
      Caption = '&Clear'
      ShortCut = 16451
      OnExecute = actClearExecute
    end
  end
  object MainMenu1: TMainMenu
    Left = 208
    Top = 96
    object F1: TMenuItem
      Caption = 'File'
      object O1: TMenuItem
        Action = actLoad
      end
      object S1: TMenuItem
        Action = actSave
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Action = actClose
      end
    end
    object G1: TMenuItem
      Caption = 'Game of Life'
      object S2: TMenuItem
        Action = actStep
      end
      object miPlay: TMenuItem
        Action = actPlay
      end
      object Clear1: TMenuItem
        Action = actClear
      end
      object P2: TMenuItem
        Action = actRandom
      end
      object Edit1: TMenuItem
        Action = actEdit
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object R1: TMenuItem
        Action = actRules
      end
      object O2: TMenuItem
        Action = actOptions
      end
    end
    object Dev1: TMenuItem
      Caption = 'Dev'
      object Benchmark1: TMenuItem
        Action = actBenchmark
      end
    end
  end
end
