unit uRules;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxRadioGroup, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxClasses, cxGridCustomView, cxGrid;

type
  TfrmRules = class(TForm)
    grRules: TcxGrid;
    grRulesTableView1: TcxGridTableView;
    grRulesNeighbours: TcxGridColumn;
    grRulesAction: TcxGridColumn;
    grRulesLevel2: TcxGridLevel;
    procedure grRulesActionPropertiesChange(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  frmRules: TfrmRules;

implementation

{$R *.dfm}

uses uMain;

procedure TfrmRules.grRulesActionPropertiesChange(Sender: TObject);
var
  i: Integer;
  v: Integer;
begin
  frmMain.GOL.RuleBirth := [];
  frmMain.GOL.RuleDeath := [];
  for i := 0 to 8 do
  begin
    v := grRulesTableView1.DataController.Values[i, 1];
    case v of
      0:
        frmMain.GOL.RuleBirth := frmMain.GOL.RuleBirth + [i];
      1:
        frmMain.GOL.RuleDeath := frmMain.GOL.RuleDeath + [i];
    end;
  end;
end;

end.
