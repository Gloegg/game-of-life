unit uFrmEdit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uGOL, Vcl.StdCtrls, Vcl.Grids, AdvObj, BaseGrid, AdvGrid;

type
  TfrmEdit = class(TForm)
    grCells: TAdvStringGrid;
    btClear: TButton;
    btOK: TButton;
    procedure btClearClick(Sender: TObject);
    procedure grCellsClickCell(Sender: TObject; ARow, ACol: Integer);
  private
    { Private-Deklarationen }
    fGOL : TGOL;
  public
    { Public-Deklarationen }
    procedure setGOL(aGOL :TGOL);
  end;

var
  frmEdit: TfrmEdit;

implementation

{$R *.dfm}

procedure TfrmEdit.btClearClick(Sender: TObject);
var
  i: Integer;
  j: Integer;
begin
  fGOL.Clear;
  for i := 0 to fGOL.Width-1 do
  begin
    for j := 0 to fGOL.Height-1 do
    begin
//      fGOL.SetAlive(i,j,false);
      grCells.Cells[i,j] := '';
    end;
  end;
end;

procedure TfrmEdit.grCellsClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  fGOL.SetAlive(aCol, aRow, not fGOL.IsAlive(aCol,aRow));
  if fGOL.IsAlive(aCol,aRow) then
    grCells.Cells[aCol, aRow] := 'x'
  else
    grCells.Cells[aCol, aRow] := ''
end;

procedure TfrmEdit.setGOL(aGOL: TGOL);
var
  x: Integer;
  y: Integer;
begin
  fGOL := aGOL;
  grCells.ColCount := fGOL.Width;
  grCells.RowCount := fGOL.Height;
  for x := 0 to fGOL.Width-1 do
  begin
    for y := 0 to fGOL.Height-1 do
    begin
      if fGOL.IsAlive(x,y) then
        grCells.Cells[x,y] := 'x';
    end;
  end;
end;

end.
