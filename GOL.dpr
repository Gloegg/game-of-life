program GOL;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  uGOL in 'uGOL.pas',
  uRules in 'uRules.pas' {frmRules},
  uFrmEdit in 'uFrmEdit.pas' {frmEdit};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmRules, frmRules);
  Application.Run;
end.
