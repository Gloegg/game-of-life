unit uMain;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  Vcl.StdCtrls,
  uGOL,
  Vcl.ActnList,
  Vcl.Menus,
  Vcl.StdActns,
  UOpenGLCanvas;

type
  TDrawPanel = class(TWinControl)
  published
    property OnResize;
    property OnMouseDown;
    property OnMouseMove;
    // property OnMouseUp;
  end;

  TfrmMain = class(TForm)
    sdFile: TSaveDialog;
    odFile: TOpenDialog;
    ActionList1: TActionList;
    actStep: TAction;
    actPlay: TAction;
    actPause: TAction;
    actRandom: TAction;
    actSave: TAction;
    actLoad: TAction;
    MainMenu1: TMainMenu;
    F1: TMenuItem;
    G1: TMenuItem;
    O1: TMenuItem;
    S1: TMenuItem;
    S2: TMenuItem;
    miPlay: TMenuItem;
    P2: TMenuItem;
    R1: TMenuItem;
    O2: TMenuItem;
    actRules: TAction;
    actOptions: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    Exit1: TMenuItem;
    actClose: TAction;
    actBenchmark: TAction;
    Dev1: TMenuItem;
    Benchmark1: TMenuItem;
    actEdit: TAction;
    Edit1: TMenuItem;
    actClear: TAction;
    Clear1: TMenuItem;
    Panel1: TPanel;
    lbFPS: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actStepExecute(Sender: TObject);
    procedure actPlayExecute(Sender: TObject);
    procedure actRandomExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actLoadExecute(Sender: TObject);
    procedure actCloseExecute(Sender: TObject);
    procedure actRulesExecute(Sender: TObject);
    procedure actBenchmarkExecute(Sender: TObject);
    procedure actEditExecute(Sender: TObject);
    procedure actClearExecute(Sender: TObject);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
  private
    fGOL: TGOL;
    fRunning: Boolean;
    fGLCanvas: TGLCanvas;
    fDP: TDrawPanel;
    lastp: TGLPointF;
    fFrames: integer;
    procedure Render;
    procedure OnResize(Sender: TObject);
    procedure DrawPanelMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
    procedure DrawPanelMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
  public
    property GOL: TGOL read fGOL;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  System.Diagnostics,
  uRules,
  uFrmEdit,
  Math;

procedure TfrmMain.actLoadExecute(Sender: TObject);
begin
  if odFile.Execute then
  begin
    fGOL.LoadFromFile(odFile.FileName);
    ClientWidth := fGOL.Width * 2;
    ClientHeight := fGOL.Height * 2;
    Render;
  end;
end;

procedure TfrmMain.actPlayExecute(Sender: TObject);
begin
  if not fRunning then
  begin
    TThread.CreateAnonymousThread(
      procedure
      var
        start, t, freq: Int64;
      begin
        QueryPerformanceFrequency(freq);
        freq := freq div 2;
        QueryPerformanceCounter(start);
        while fRunning do
        begin
          fGOL.Evolve;
          TThread.Synchronize(TThread.CurrentThread,
            procedure
            begin
              Render;
              QueryPerformanceCounter(t);
              if t - start > freq then
              begin
                lbFPS.Caption := Format('FPS: %d', [fFrames * 2]);
                start := t;
                fFrames := 0;
              end;
            end);
          // sleep(5);
        end;
      end).start;

    fRunning := true;
    miPlay.Action := actPause;
  end
  else
  begin
    fRunning := false;
    miPlay.Action := actPlay;
  end;
end;

procedure TfrmMain.actRandomExecute(Sender: TObject);
begin
  fGOL.FillRandom;
  Render;
end;

procedure TfrmMain.actRulesExecute(Sender: TObject);
begin
  frmRules.Show;
end;

procedure TfrmMain.actSaveExecute(Sender: TObject);
begin
  if sdFile.Execute then
  begin
    fGOL.SaveToFile(sdFile.FileName);
  end;
end;

procedure TfrmMain.actStepExecute(Sender: TObject);
begin
  fGOL.Evolve;
  Render;
end;

procedure TfrmMain.DrawPanelMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  ActiveControl := nil;
  if Button = mbRight then
  begin
    lastp.X := X;
    lastp.Y := Y;
  end;
end;

procedure TfrmMain.DrawPanelMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
var
  diffX, diffY: Double;
begin
  if ssRight in Shift then
  begin
    diffX := lastp.X - X;
    diffY := lastp.Y - Y;

    fGLCanvas.BeginUpdateTransformation;
    fGLCanvas.TranslateX := fGLCanvas.TranslateX - diffX;
    fGLCanvas.TranslateY := fGLCanvas.TranslateY - diffY;
    fGLCanvas.EndUpdateTransformation;
    lastp.X := X;
    lastp.Y := Y;
    Render;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  fRunning := false;

  fDP := TDrawPanel.Create(Panel1);
  fDP.Parent := Panel1;
  fDP.Align := alClient;
  fDP.OnResize := OnResize;
  fDP.OnMouseDown := DrawPanelMouseDown;
  fDP.OnMouseMove := DrawPanelMouseMove;

  fGOL := TGOL.Create(ClientWidth div 2, ClientHeight div 2);
  // fGOL := TGOL.Create(5,5);

  fGLCanvas := TGLCanvas.Create(fDP, false, true, false, true, false);
  fGLCanvas.DefaultFont.WinColor := clBlack;

  actClose.ShortCut := ShortCut(VK_F4, [ssAlt]);
  Render;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  fGOL.Free;
end;

procedure TfrmMain.FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  fGLCanvas.SetEqualScale(Max(fGLCanvas.ScaleX * 0.9, 0.01));
  Render;
end;

procedure TfrmMain.FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  fGLCanvas.SetEqualScale(Max(fGLCanvas.ScaleX * 1.1, 0.01));
  Render;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  OnResize(fDP);
end;

procedure TfrmMain.OnResize(Sender: TObject);
var
  ScaleX, scaleY, scale: Double;
  transX, transY: Double;
begin
  if Assigned(fGLCanvas) then
  begin
    ScaleX := fDP.Width / fGOL.Width;
    scaleY := fDP.Height / fGOL.Height;
    scale := Min(ScaleX, scaleY);
    transX := fDP.Width / 2;
    transY := fDP.Height / 2;
    fGLCanvas.BeginUpdateTransformation.ResetTransformation;
    fGLCanvas.SetEqualScale(scale);
    fGLCanvas.SetTranslateX(transX);
    fGLCanvas.SetTranslateY(transY);
    fGLCanvas.EndUpdateTransformation;
    Render;
  end;
end;

procedure TfrmMain.Render;
var
  X, Y: integer;
  c: TColor;
begin
  fGLCanvas.RenderingBegin(clGray);
  for X := 0 to fGOL.Width - 1 do
  begin
    for Y := 0 to fGOL.Height - 1 do
    begin
      if fGOL.IsAlive(X, Y) then
      begin
        if fGOL.WasAlive(X, Y) then
          c := clGreen
        else
          c := clLime;
      end
      else
      begin
        if fGOL.WasAlive(X, Y) then
          c := $A0B69E
        else
          c := clBlack;
      end;
      fGLCanvas.SetBrushColorWin(c, 255, false);
      fGLCanvas.FillRect(X - fGOL.Width div 2 - 1, Y - fGOL.Height div 2 - 1, X - fGOL.Width div 2 + 1, Y - fGOL.Height div 2 + 1);
    end;
  end;
  fGLCanvas.RenderingEnd;
  Caption := Format('GOL Generation: %d', [fGOL.Generation]);
  inc(fFrames);
end;

procedure TfrmMain.actBenchmarkExecute(Sender: TObject);
var
  sw: TStopwatch;
  i: integer;
begin
  sw := TStopwatch.Create;
  sw.start;
  for i := 1 to 100 do
  begin
    fGOL.Evolve;
  end;
  sw.Stop;
  ShowMessage(Format('%.3fs', [sw.ElapsedMilliseconds / 1000]));
  Render;
end;

procedure TfrmMain.actClearExecute(Sender: TObject);
begin
  fGOL.Clear;
  Render;
end;

procedure TfrmMain.actCloseExecute(Sender: TObject);
begin
  fRunning := false;
  Close;
end;

procedure TfrmMain.actEditExecute(Sender: TObject);
begin
  Application.CreateForm(TfrmEdit, frmEdit);
  frmEdit.setGOL(fGOL);
  frmEdit.ShowModal;
  frmEdit.Free;
  Render;
end;

end.
