object frmRules: TfrmRules
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'Rules'
  ClientHeight = 476
  ClientWidth = 133
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    133
    476)
  PixelsPerInch = 96
  TextHeight = 13
  object grRules: TcxGrid
    Left = 8
    Top = 8
    Width = 117
    Height = 460
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ExplicitWidth = 583
    ExplicitHeight = 552
    object grRulesTableView1: TcxGridTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.Data = {
        D10000000F00000044617461436F6E74726F6C6C657231020000001300000054
        6378496E746567657256616C75655479706514000000546378536D616C6C696E
        7456616C75655479706509000000445855464D54000000000000000100445855
        464D54000001000000000100445855464D54000002000000000200445855464D
        54000003000000000000445855464D54000004000000000100445855464D5400
        0005000000000100445855464D54000006000000000100445855464D54000007
        000000000100445855464D54000008000000000100}
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object grRulesNeighbours: TcxGridColumn
        Caption = 'Neighbours'
        DataBinding.ValueType = 'Integer'
        Options.Editing = False
        Width = 60
      end
      object grRulesAction: TcxGridColumn
        Caption = 'Action'
        DataBinding.ValueType = 'Smallint'
        PropertiesClassName = 'TcxRadioGroupProperties'
        Properties.ImmediatePost = True
        Properties.Items = <
          item
            Caption = 'Birth'
            Value = 0
          end
          item
            Caption = 'Death'
            Value = 1
          end
          item
            Caption = 'None'
            Value = 2
          end>
        Properties.OnChange = grRulesActionPropertiesChange
        Width = 51
      end
    end
    object grRulesLevel2: TcxGridLevel
      GridView = grRulesTableView1
    end
  end
end
