unit uGOL;

interface

uses
  Generics.Collections,
  Windows;

type
  TCell = class
  strict private
    fIsAlive: boolean;
    fIsEvolved: boolean;
    fNeighbours: TArray<TCell>;
    fNext: TCell;
    fAlive: byte;
  public
    constructor Create(aAlive: boolean);
    property Next: TCell read fNext write fNext;
    property Neighbours: TArray<TCell> read fNeighbours;
    property IsAlive: boolean read fIsAlive write fIsAlive;
    property IsEvolved: boolean read fIsEvolved write fIsEvolved;
    property AliveNeighbours: byte read fAlive write fAlive;

    procedure AddNeighbour(c: TCell);
    function Evolve: boolean;
    procedure RecalcNeighbours;
    function getLivingNeighbours: byte;
    procedure Birth;
    procedure Death;
    procedure PrepareCell; inline;
  end;

  TGOLRule = 0 .. 8;
  TGOLRules = set of TGOLRule;

  TGOL = class
  strict private
    fWidth, fHeight: integer;
    fField: array [0 .. 1] of array of array of TCell;
    fActive: byte;
    fGeneration: integer;
    fBirth: TGOLRules;
    fDeath: TGOLRules;
    fChanges: TList<TCell>;
    fChanged: boolean;
    procedure InitField;
    procedure EvolveAll;
    procedure PrepareCells;
  public
    constructor Create; overload;
    constructor Create(aWidth, aHeight: integer); overload;
    destructor Destroy; override;

    property Width: integer read fWidth;
    property Height: integer read fHeight;
    property Generation: integer read fGeneration;
    property RuleBirth: TGOLRules read fBirth write fBirth;
    property RuleDeath: TGOLRules read fDeath write fDeath;


    procedure Evolve;
    function IsAlive(X, Y: integer): boolean; inline;
    function WasAlive(X, Y: integer): boolean; inline;

    procedure FillRandom;
    procedure Clear;

    procedure SaveToFile(const aFilename: string);
    procedure LoadFromFile(const aFilename: string);

    procedure SetAlive(X, Y: integer; aAlive: boolean);
  end;

implementation

uses
  IOUtils,
  SysUtils,
  Dialogs,
  JSON;

{ TGOL }

procedure TGOL.Clear;
var
  X: integer;
  Y: integer;
begin
  for X := 0 to fWidth - 1 do
    for Y := 0 to fHeight - 1 do
    begin
      fField[0, X, Y].Free;
      fField[1, X, Y].Free;
    end;

  SetLength(fField[0], 0);
  SetLength(fField[1], 0);
  InitField;
  fGeneration := 1;
  fChanges.Clear;
end;

constructor TGOL.Create(aWidth, aHeight: integer);
begin
  Create;
  fWidth := aWidth;
  fHeight := aHeight;
  InitField;
  FillRandom;
end;

constructor TGOL.Create;
begin
  fGeneration := 1;
  fActive := 0;
  fBirth := [3];
  fDeath := [0, 1, 4, 5, 6, 7, 8];
  fChanges := TList<TCell>.Create;
end;

destructor TGOL.Destroy;
begin
  fChanges.Free;
  inherited;
end;

procedure TGOL.Evolve;
var
  c, n, n2: TCell;
  changes: TList<TCell>;
begin
  if (fGeneration = 1) or fChanged then // Brute-Force evolve in first generation
  begin
    EvolveAll;
    fChanged := false;
  end
  else // only evolve changed cells and their neighbors in succeeding generations
  begin
    changes := TList<TCell>.Create;
    changes.Capacity := fWidth * fHeight div 10; // approx. 10% changes between generations

    for c in fChanges do
    begin
      if not c.IsEvolved then
        if c.Evolve then
          changes.Add(c.Next);

      for n in c.Neighbours do
      begin
        if not n.IsEvolved then
          if n.Evolve then
            changes.Add(n.Next);
      end;
    end;

    // Prepare changed cells for next evolution
    for c in fChanges do
    begin
      if c.IsEvolved then
      begin
        c.PrepareCell;
        c.Next.IsEvolved := false;
      end;

      // Prepare changed cells-neighbours for next evolution
      for n in c.Neighbours do
      begin
        if n.IsEvolved then
        begin
          n.PrepareCell;
          n.Next.IsEvolved := false;
        // Prepare changed cells-neighbours-neighbours for next evolution
        for n2 in n.Neighbours do
        begin
          n2.AliveNeighbours := n2.Next.AliveNeighbours;
        end;
        end;
      end;
    end;

    // swap new changes with old changes
    fChanges.Free;
    fChanges := changes;

    fActive := (fActive + 1) mod 2;
    Inc(fGeneration);
  end;
end;

procedure TGOL.EvolveAll;
var
  X: integer;
  Y: integer;
begin
  fChanges.Clear;

  for X := 0 to fWidth - 1 do
  begin
    for Y := 0 to fHeight - 1 do
    begin
      if fField[fActive, X, Y].Evolve then
        fChanges.Add(fField[fActive, X, Y].Next);
    end;
  end;
  PrepareCells;
  fActive := (fActive + 1) mod 2;
  Inc(fGeneration);
end;

procedure TGOL.FillRandom;
var
  X, Y: integer;
  bAlive: boolean;
begin
  Clear;
  for X := 0 to fWidth - 1 do
  begin
    for Y := 0 to fHeight - 1 do
    begin
      bAlive := random(100) < 10;
      fField[0, X, Y].IsAlive := bAlive;
      fField[1, X, Y].IsAlive := bAlive;
      if bAlive then
      begin
        fField[0, X, Y].Birth;
        fField[1, X, Y].Birth;
      end;
    end;
  end;
  fGeneration := 1;
end;

procedure TGOL.InitField;
const
  Neighbours: array [1 .. 8] of TPoint = ((X: - 1; Y: 1), (X: 0; Y: 1), (X: 1; Y: 1), (X: - 1; Y: 0), (X: 1; Y: 0), (X: - 1; Y: - 1), (X: 0; Y: - 1),
    (X: 1; Y: - 1));
var
  X, Y: integer;
  pt: TPoint;
begin
  SetLength(fField[0], fWidth, fHeight);
  SetLength(fField[1], fWidth, fHeight);

  // Setup Cells and their counterpart
  for X := 0 to fWidth - 1 do
  begin
    for Y := 0 to fHeight - 1 do
    begin
      fField[0, X, Y] := TCell.Create(false);
      fField[1, X, Y] := TCell.Create(false);
      fField[0, X, Y].Next := fField[1, X, Y];
      fField[1, X, Y].Next := fField[0, X, Y];
    end;
  end;

  // Setup Neighborhood
  for X := 0 to fWidth - 1 do
  begin
    for Y := 0 to fHeight - 1 do
    begin
      for pt in Neighbours do
      begin
        if (X + pt.X >= 0) and (X + pt.X < fWidth) and (Y + pt.Y >= 0) and (Y + pt.Y < fHeight) then
        begin
          fField[0, X, Y].AddNeighbour(fField[0, X + pt.X, Y + pt.Y]);
          fField[1, X, Y].AddNeighbour(fField[1, X + pt.X, Y + pt.Y]);
        end;
      end;
    end;
  end;
end;

function TGOL.IsAlive(X, Y: integer): boolean;
begin
  result := fField[fActive, X, Y].IsAlive;
end;

procedure TGOL.LoadFromFile(const aFilename: string);
var
  s: string;
  i, X, Y: integer;
  jo: IJSONObject;
  jaRules, jaRows, jaCells: IJSONArray;
begin
  Clear;
  s := TFile.ReadAllText(aFilename);
  jo := TJSON.NewObject(s);

  fWidth := jo.GetInteger('Width');
  fHeight := jo.GetInteger('Height');
  fGeneration := jo.GetInteger('Generation');
  InitField;

  jaRules := jo.GetJSONArray('Birth');
  for i := 0 to jaRules.Count - 1 do
  begin
    fBirth := fBirth + [jaRules.GetInteger(i)];
  end;
  jaRules := jo.GetJSONArray('Death');
  for i := 0 to jaRules.Count - 1 do
  begin
    fDeath := fDeath + [jaRules.GetInteger(i)];
  end;
  jaRows := jo.GetJSONArray('Data');
  for Y := 0 to jaRows.Count - 1 do
  begin
    jaCells := jaRows.GetJSONArray(Y);
    for X := 0 to jaCells.Count - 1 do
    begin
      fField[fActive, X, Y].IsAlive := jaCells.GetBoolean(X);
      fField[fActive, X, Y].Next.IsAlive := jaCells.GetBoolean(X);
      fField[fActive, X, Y].RecalcNeighbours;
      fField[fActive, X, Y].Next.RecalcNeighbours;
    end;
  end;
  fChanged := true;
end;

procedure TGOL.PrepareCells;
var
  X: integer;
  Y: integer;
begin
  for X := 0 to fWidth - 1 do
    for Y := 0 to fHeight - 1 do
    begin
      fField[0, X, Y].IsEvolved := false;
      fField[1, X, Y].IsEvolved := false;
      fField[fActive, X, Y].AliveNeighbours := fField[fActive, X, Y].Next.AliveNeighbours;
    end;
end;

procedure TGOL.SaveToFile(const aFilename: string);
var
  jo: IJSONObject;
  jaRules, jaRows, jaCells: IJSONArray;
  i, X, Y: integer;
begin
  jo := TJSON.NewObject;
  jo.Put('Width', fWidth);
  jo.Put('Height', fHeight);
  jo.Put('Generation', fGeneration);
  jaRules := TJSON.NewArray;
  for i := 0 to 8 do
    if i in fBirth then
      jaRules.Put(i);
  jo.Put('Birth', jaRules);
  jaRules := TJSON.NewArray;
  for i := 0 to 8 do
    if i in fDeath then
      jaRules.Put(i);
  jo.Put('Death', jaRules);

  jaRows := TJSON.NewArray;
  for Y := 0 to fHeight - 1 do
  begin
    jaCells := TJSON.NewArray;
    for X := 0 to fWidth - 1 do
    begin
      jaCells.Put(IsAlive(X, Y));
    end;
    jaRows.Put(jaCells);
  end;

  jo.Put('Data', jaRows);

  TFile.WriteAllText(aFilename, jo.ToString(false));
end;

procedure TGOL.SetAlive(X, Y: integer; aAlive: boolean);
begin
  fChanged := true;
  fField[fActive, X, Y].IsAlive := aAlive;
  fField[fActive, X, Y].Next.IsAlive := aAlive;
  fField[fActive, X, Y].RecalcNeighbours;
  fField[fActive, X, Y].Next.RecalcNeighbours;
end;

function TGOL.WasAlive(X, Y: integer): boolean;
begin
  result := fField[fActive, X, Y].Next.IsAlive;
end;

{ TCell }

procedure TCell.AddNeighbour(c: TCell);
begin
  if c <> Self then
  begin
    SetLength(fNeighbours, length(fNeighbours) + 1);
    fNeighbours[high(fNeighbours)] := c;
    if c.IsAlive then
      Inc(fAlive);
  end;
end;

procedure TCell.Birth;
var
  c: TCell;
begin
  for c in fNeighbours do
  begin
    c.AliveNeighbours := c.AliveNeighbours + 1;
  end;
end;

constructor TCell.Create(aAlive: boolean);
begin
  SetLength(fNeighbours, 0);
  fIsAlive := aAlive;
  fAlive := 0;
end;

procedure TCell.Death;
var
  c: TCell;
begin
  for c in fNeighbours do
  begin
    c.AliveNeighbours := c.AliveNeighbours - 1;
  end;
end;

function TCell.Evolve: boolean;
begin
  case fAlive of
    2:
      fNext.IsAlive := IsAlive;
    3:
      fNext.IsAlive := true;
  else
    fNext.IsAlive := false;
  end;
  fIsEvolved := true;
  result := fIsAlive <> fNext.IsAlive;
  if result then
  begin
    if fNext.IsAlive then
      fNext.Birth
    else
      fNext.Death;
  end;
end;

function TCell.getLivingNeighbours: byte;
var
  c: TCell;
begin
  result := 0;
  for c in fNeighbours do
    if c.IsAlive then
      Inc(result);
end;

procedure TCell.PrepareCell;
begin
  fIsEvolved := false;
  fAlive := fNext.AliveNeighbours;
end;

procedure TCell.RecalcNeighbours;
var
  c: TCell;
begin
  for c in fNeighbours do
  begin
    c.AliveNeighbours := c.getLivingNeighbours;
  end;
end;

initialization

begin
  randomize;
end;

end.
